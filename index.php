<?php require "nif.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ZS | Utilitários NIF</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js" integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js" integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
</head>
<body>
    <div class="container mt-5">
        <div class="row">
            <div class="col-6">
                <div class="card mt-5">
                    <div class="row">
                        <div class="col">
                            <div class="card-header">
                                <h5>Validar NIF</h5>
                            </div>
                            <div class="card-body">
                                <div class="input-group mb-3">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text" id="basic-addon1">NIF</span>
                                    </div>
                                    <input type="text" class="form-control" name="nif" id="nif" placeholder="Introduza o nif a validar" aria-label="Username" aria-describedby="basic-addon1">
                                </div>
                                <div class="card text-center" id="validateResult">
                                    <div class="card-body">
                                        <h5>...</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-end">
                                <button type="submit" class="btn btn-primary ml-3 align-end" name="validate" id="validate" onclick="validate()">Validar</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-6">
                <div class="card mt-5">
                    <div class="row">
                        <div class="col">
                            <div class="card-header">
                                <h5>Gerar NIF</h5>
                            </div>
                            <div class="card-body">
                                <div class="input-group mb-3">
                                    <label class="input-group-text" for="init">Tipo de NIF</label>
                                    <select class="form-select" name="init" id="init">
                                        <?php foreach ($initRange as $k => $value) { ?>
                                            <option value="<?php echo $k ?>"><?php echo $k ?> - <?php echo $value ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                                <div class="card text-center" id="generateResult">
                                    <div class="card-body">
                                        <h5>...</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer text-end">
                                <div class="row">
                                    <div class="col-4 text-start">
                                        <span class='copied'></span>
                                    </div>
                                    <div class="col-8">
                                        <button class="btn btn-outline-dark ml-3" id="copy" onclick="copy()">Copiar</button>
                                        <button type="submit" class="btn btn-primary ml-3" name="generate" id="generate" onclick="generate()">Gerar</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script src="script.js"></script>
</body>
</html>