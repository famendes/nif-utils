<?php

    function validateNif($nif, $operation)
    {
        $digits = preg_split('//', $nif, -1, PREG_SPLIT_NO_EMPTY);
        $sum = 0;

        if ($operation == 'val')
        {
            $lastDigit = $digits[count($digits)-1];
            $pos = count($digits) - 2;
        }
        else
        {
            $pos = count($digits) -1 ;
        }
        
        for ($i = 2; $i <= 9; $i++)
        {   
            $sum = $sum + ($digits[$pos] * $i);
            if ($pos > 0) 
            {
                $pos--;
            }
        }

        if ($sum % 11 == 0 || $sum % 11 == 1)
        {
            $cDigit = 0;
        }
        else 
        {
            $cDigit = 11 - ($sum % 11);
        }

        if ($operation == 'val')
        {
            if ($lastDigit == $cDigit)
            {
                return "Nif válido!";
            }
            else 
            {
                return "Nif Inválido!";
            }
        }
        else 
        {
            return $cDigit;
        }
        
    }

    function generateNif($init)
    {
        $preNif = null;
        if (strlen(strval($init)) == 1) 
        {
            switch($init)
            {
                case 1:
                    $preNif = rand($init*10000000, 19999999);
                    break;
                case 2:
                    $preNif = rand($init*10000000, 29999999);
                    break;
                case 3:
                    $preNif = rand($init*10000000, 39999999);
                    break;
                case 5:
                    $preNif = rand($init*10000000, 59999999);
                    break;
                case 6:
                    $preNif = rand($init*10000000, 69999999);
                    break;
            }
        }
        else 
        {
            switch($init)
            {
                case 45:
                    $preNif = rand($init*1000000, 45999999);
                    break;
                case 70:
                    $preNif = rand($init*1000000, 70999999);
                    break;
                case 74:
                    $preNif = rand($init*1000000, 74999999);
                    break;
                case 75:
                    $preNif = rand($init*1000000, 75999999);
                    break;
                case 71:
                    $preNif = rand($init*1000000, 71999999);
                    break;
                case 72:
                    $preNif = rand($init*1000000, 72999999);
                    break;
                case 77:
                    $preNif = rand($init*1000000, 77999999);
                    break;
                case 78:
                    $preNif = rand($init*1000000, 78999999);
                    break;
                case 79:
                    $preNif = rand($init*1000000, 79999999);
                    break;
                case 90:
                    $preNif = rand($init*1000000, 90999999);
                    break;
                case 91:
                    $preNif = rand($init*1000000, 91999999);
                    break;
                case 98:
                    $preNif = rand($init*1000000, 98999999);
                    break;
                case 99:
                    $preNif = rand($init*1000000, 99999999);
                    break;
            }
        }
        return $preNif;
    }

    $initRange = array(
        1 => "Pessoa singular",
        2 => "Pessoa singular",
        3 => "Pessoa singular",
        5 => "Pessoa coletiva",
        6 => "Pessoa coletiva público",
        45 => "Pessoa singular", 
        70 => "Herança Indivisa", 
        74 => "Herança Indivisa", 
        75 => "Herança Indivisa", 
        71 => "Não residentes colectivos sujeitos a retenção na fonte a título definitivo", 
        72 => "Fundos de investimento", 
        77 => "Atribuição Oficiosa de NIF de sujeito passivo (entidades que não requerem NIF junto do RNPC)", 
        78 => "Atribuição oficiosa a não residentes abrangidos pelo processo VAT REFUND", 
        79 => "Regime excepcional - Expo 98", 
        90 => "Condomínios, Sociedade Irregulares, Heranças Indivisas cujo autor da sucessão era empresário individual", 
        91 => "Condomínios, Sociedade Irregulares, Heranças Indivisas cujo autor da sucessão era empresário individual", 
        98 => "Não residentes sem estabelecimento estável", 
        99 => "Sociedades civis sem personalidade jurídica"
    );

    if (isset($_POST['nif']) && strlen($_POST['nif']) == 9)
    {   
        $stringNif = strval($_POST['nif']);
        echo validateNif($stringNif, 'val');
    }
    else if (isset($_POST['nif']) && strlen($_POST['nif']) != 9)
    {
        echo "Formato de NIF inválido para Portugal.";
    }
    

    if (isset($_POST['init']))
    {
        $stringInit = strval($_POST['init']);
        $preNif = generateNif($stringInit);
        $newNif = $preNif . '' . validateNif($preNif , 'gen');
        echo $newNif;
    }
?>