$(document).ready(() => {
    $("#copy").hide();
})

function validate() {
    if ($('#nif').val() == '') {
        alert("Indique um NIF para validação!!")
    }
    else {
        let data = $('#nif').serialize()
        $.ajax({
        type: 'POST',
        url: 'nif.php',
        data: data,
        success: response => {
            $("#validateResult").show();
            $("#validateResult .card-body h5").html(response);
            $("#copy").attr('disabled', true);
        },
        error: error => { console.log(error)}
    })
    }
}

function generate() {
    
    let data = $('#init').serialize()
    $.ajax({
        type: 'POST',
        url: 'nif.php',
        data: data,
        success: response => {
            $("#copy").show();
            $("#generateResult").show();
            $("#generateResult .card-body h5").html(response);
            $("#copy").attr('disabled', false);
        },
        error: error => { console.log(error)}
    })
}

function copy() {
    var text = document.getElementById('generateResult').innerText;
    var dummy = document.createElement("textarea");
    document.body.appendChild(dummy);
    dummy.value = text;
    dummy.select();
    document.execCommand("copy");
    $(".copied").text("Copiado!").show().fadeOut(2500);
    document.body.removeChild(dummy);
}